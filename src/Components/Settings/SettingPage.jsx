import React, { useEffect, useState } from "react";
import UsersInfo from "../Tools/UserInfo";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faTrash } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Modal from "../Tools/Modal";
import { useNavigate } from "react-router-dom";
const SettingPage = () => {
  const [prenom, setPrenom] = useState({ prenom: "" });
  const [nom, setNom] = useState({ nom: "" });
  const [email, setEmail] = useState({ email: "" });
  const [showPasswordModal, setShowPasswordModal] = useState(false);
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");
  const [messageError, setMessageError] = useState("");
  const [showConfMessage, setShowConfMessage] = useState(false);
  const history = useNavigate();
  const modifyUser = async (e) => {
    e.preventDefault();
    const { data } = await axios.put(
      `${import.meta.env.VITE_API_BASE_URL}/update/user`,
      {
        nom: nom.nom,
        prenom: prenom.prenom,
        email: email.email,
        id_user: UsersInfo.id,
      }
    );

    fetchAllUsers();
    if (data?.success !== "") {
      toast.success(data?.success);
    }
  };

  const updatePassword = async () => {
    const { data } = await axios.put(
      `${import.meta.env.VITE_API_BASE_URL}/password/update`,
      {
        password1,
        password2,
        id_user: UsersInfo.id,
      }
    );
    setMessageError(data?.message);
    if (data?.success !== "") {
      toast.success(data?.success);
      setShowPasswordModal(false);
    }
  };

  const fetchAllUsers = async () => {
    const { data } = await axios.get(
      `${import.meta.env.VITE_API_BASE_URL}/user/${UsersInfo.id}`
    );

    setPrenom({ prenom: data.prenom });
    setNom({ nom: data.nom });
    setEmail({ email: data.email });
  };

  useEffect(() => {
    fetchAllUsers();
  }, []);

  const showConfirmationMessage = (e) => {
    e.stopPropagation();
    setShowConfMessage((prev) => !prev);
  };

  const deleteByAnswer = async (answer) => {
    if (answer === "ok") {
      try {
        await axios.delete(
          `${import.meta.env.VITE_API_BASE_URL}/account/delete/${UsersInfo.id}`
        );
        history("/login");
      } catch (error) {
        console.error(error);
      }
    }
    setShowConfMessage(false);
  };

  return (
    <div className="w-full mt-8  flex flex-col ">
      <ToastContainer autoClose={1500} />
      <form
        onSubmit={modifyUser}
        className="bg-white shadow-md p-3 border rounded w-[80%] mx-auto"
      >
        <div className="flex items-center p-2 w-full  gap-5">
          <div className="w-1/2">
            <label htmlFor="prn" className="font-semibold">
              First name
            </label>
            <input
              type="text"
              value={prenom.prenom}
              id="prn"
              onChange={(e) => setPrenom({ prenom: e.target.value })}
              placeholder="example@test.com"
              className="bg-white p-1 w-full shadow px-1 rounded "
              required
            />
          </div>
          <div className="w-1/2">
            <label htmlFor="Nom" className="font-semibold">
              Last name
            </label>
            <input
              type="text"
              id="Nom"
              value={nom.nom}
              onChange={(e) => setNom({ nom: e.target.value })}
              placeholder="example@test.com"
              className="bg-white p-1 w-full shadow px-1 rounded "
              required
            />
          </div>
        </div>

        <div className="flex flex-col p-2  gap-3">
          <label htmlFor="eml" className="font-semibold">
            Email{" "}
          </label>
          <input
            type="email"
            id="eml"
            value={email.email}
            onChange={(e) => setEmail({ email: e.target.value })}
            placeholder="example@test.com"
            className="bg-white p-1 w-full shadow px-1 rounded "
            required
          />
        </div>
        <div className="w-full ml-2">
          <button className="bg-blue-600 text-white font-bold p-1.5 text-base mt-5 rounded">
            Update
          </button>
        </div>
        <div className="flex flex-col p-2  gap-3 ml-1">
          <div className="flex items-center gap-3 w-full  mt-5">
            <div
              onClick={() => setShowPasswordModal((prev) => !prev)}
              className=" p-1.5 cursor-pointer underline flex items-center gap-2 text-blue-600 "
            >
              <FontAwesomeIcon icon={faEdit} className="w-4 h-4" />
              Change password
            </div>

            <div
              onClick={showConfirmationMessage}
              className="bg-red-500 text-white p-1 flex items-center gap-2 justify-center rounded-md w-44  cursor-pointer hover:opacity-70 duration-100 transition-all"
            >
              Delete account
              <FontAwesomeIcon icon={faTrash} className="w-4 h-4" />
            </div>
          </div>
        </div>
      </form>

      <Modal isOpen={showPasswordModal} setIsOpen={setShowPasswordModal}>
        <div className="w-full">
          <div className="flex flex-col">
            <label htmlFor="pass" className="mb-1 font-semibold">
              New password
            </label>
            <input
              type="password"
              id="pass"
              onChange={(e) => setPassword1(e.target.value)}
              placeholder="**********"
              className="bg-white p-1 w-full shadow px-1 rounded-lg border"
              required
            />
          </div>

          <div className="flex flex-col mt-5">
            <label htmlFor="pass" className="mb-1 font-semibold">
              Comfirm password
            </label>
            <input
              type="password"
              id="pass"
              onChange={(e) => setPassword2(e.target.value)}
              placeholder="**********"
              className="bg-white p-1 w-full shadow rounded-lg  px-1  border "
              required
            />
          </div>
          <button
            onClick={updatePassword}
            className="bg-blue-600 text-white text-base p-1.5 w-44 rounded mt-5 font-medium"
          >
            Change password
          </button>
          {messageError !== "" && (
            <div className="text-red-600 mt-3 mb-2">{messageError}</div>
          )}
        </div>
      </Modal>
      <Modal isOpen={showConfMessage} setIsOpen={setShowConfMessage}>
        <div className="flex flex-col w-full text-xl font-semibold mx-auto">
          <div className="text-center my-8">
            Are you sure you want delete your account ?
          </div>
          <div className="w-full flex items-center gap-4 justify-center my-3 text-lg">
            <button
              onClick={() => deleteByAnswer("ok")}
              className="bg-[#00c48f] text-white p-1 rounded w-40 cursor-pointer hover:opacity-70 duration-100 transition-all"
            >
              Ok
            </button>
            <button
              onClick={() => deleteByAnswer("no")}
              className="bg-[#07a4e7] text-white p-1 rounded w-40 hover:opacity-70 duration-100 transition-all"
            >
              Cancel
            </button>
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default SettingPage;
