export const formateDate = (dateString) => {
  const formattedDate = new Date(dateString);
  formattedDate.setDate(formattedDate.getDate() + 1);
  const updatedDate = formattedDate.toISOString().substring(0, 10);
  return updatedDate;
};
